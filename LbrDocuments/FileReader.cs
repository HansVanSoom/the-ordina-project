﻿namespace LbrDocuments
{
    public class FileReader : IEncryptionStrategy
    {
        private Dictionary<string, List<string>> filePermissions;
        private Dictionary<string, List<string>> decryptPermissions;
        public FileReader()
        {
            filePermissions = new Dictionary<string, List<string>>
            {
                { "admin", new List<string> { "textfile.txt", "xmlfile.xml", "jsonfile.json" } },
                { "user", new List<string> { "textfile.txt" } }
            };
            decryptPermissions = new Dictionary<string, List<string>>
            {
                { "admin", new List<string> { "None", "Reverse Decryption", "Custom Decryption" } },
                { "user", new List<string> { "None" } }
            };
        }
        public string DecryptCustom(string text)
        {
            string returnValue = "";
            char[] symbolen = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.!?,;:<> {}-@€$/[]".ToCharArray();
            char[] sleutel = "Cl;K€N39Vixe[q{ak!sWy:T,OrjU/vpG}Bm<4tM.EF5SPAR?I cZ0u$L1]6bH2z-7Ddfgo@XQYn8whJ>".ToCharArray();
            int index;

            using (StringReader reader = new StringReader(text))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    foreach (char c in line)
                    {
                        try
                        {
                            //if bool...
                            index = Array.IndexOf(sleutel, c);
                            returnValue += symbolen[index];
                        }
                        catch
                        {
                            //If the used char isn't in symbols and in the key, it will keep his original value.
                            //If you log this, you can add the char in symbols and the key in the future.
                            returnValue += c;
                        }
                    }
                    returnValue += Environment.NewLine;
                }
            }
            return returnValue;
        }
        public string DecryptReverse(string text)
        {
            string returnValue = "";
            try
            {
                using (StringReader reader = new StringReader(text))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        char[] charArray = line.ToCharArray();
                        Array.Reverse(charArray);
                        returnValue += new string(charArray) + Environment.NewLine;
                    }
                }
            }
            catch (Exception ex)
            {
                returnValue = $"Error while decrypting: {ex.Message}";
            }
            return returnValue;
        }
        public string ReadFileTXT(string filePath, string userRole, string decryptionStyle)
        {
            string returnValue;
            try
            {
                //First, check if the current user has permission to read a textfile.
                if (filePermissions.ContainsKey(userRole) && filePermissions[userRole].Contains("textfile.txt"))
                {
                    string fileContent = File.ReadAllText(filePath);
                    //Second, check if the current user has permission to decrypt a textfile with this decryption style.
                    if (decryptPermissions.ContainsKey(userRole) && decryptPermissions[userRole].Contains(decryptionStyle))
                    {
                        switch (decryptionStyle)
                        {
                            case "None":
                                break;
                            case "Reverse Decryption":
                                fileContent = DecryptReverse(fileContent);
                                break;
                            case "Custom Decryption":
                                fileContent = DecryptCustom(fileContent);
                                break;
                            default:
                                fileContent = "Unknown decryption style." + Environment.NewLine + fileContent;
                                break;
                        }
                        returnValue = fileContent;
                    }
                    else
                    {
                        returnValue = "Warning: Your role has read permissions. Not decrypt permissions. " + Environment.NewLine + fileContent;
                    }
                }
                else
                {
                    returnValue = "A " + userRole + " hasn't permission to read a textfile.";
                }
            }
            catch (Exception ex)
            {
                returnValue = $"Error while reading this textfile: {ex.Message}";
            }
            return returnValue;
        }
        public string ReadFileXML(string filePath, string userRole, string decryptionStyle)
        {
            string returnValue;
            try
            {
                //First, check if the current user has permission to read a xmlfile.
                if (filePermissions.ContainsKey(userRole) && filePermissions[userRole].Contains("xmlfile.xml"))
                {
                    string fileContent = File.ReadAllText(filePath);
                    //Second, check if the current user has permission to decrypt a textfile with this decryption style.
                    if (decryptPermissions.ContainsKey(userRole) && decryptPermissions[userRole].Contains(decryptionStyle))
                    {
                        switch (decryptionStyle)
                        {
                            case "None":
                                break;
                            case "Reverse Decryption":
                                fileContent = DecryptReverse(fileContent);
                                break;
                            case "Custom Decryption":
                                fileContent = DecryptCustom(fileContent);
                                break;
                            default:
                                fileContent = "Unknown decryption style." + Environment.NewLine + fileContent;
                                break;
                        }
                        returnValue = fileContent;
                    }
                    else
                    {
                        returnValue = "Warning: Your role has read permissions. Not decrypt permissions. " + Environment.NewLine + fileContent;
                    }
                }
                else
                {
                    returnValue = "A " + userRole + " hasn't permission to read a xmlfile.";
                }
            }
            catch (Exception ex)
            {
                returnValue = $"Error while reading this xmlfile: {ex.Message}";
            }
            return returnValue;
        }
        public string ReadFileJSON(string filePath, string userRole, string decryptionStyle)
        {
            string returnValue;
            try
            {
                //First, check if the current user has permission to read a jsonfile.
                if (filePermissions.ContainsKey(userRole) && filePermissions[userRole].Contains("jsonfile.json"))
                {
                    string fileContent = File.ReadAllText(filePath);
                    //Second, check if the current user has permission to decrypt a textfile with this decryption style.
                    if (decryptPermissions.ContainsKey(userRole) && decryptPermissions[userRole].Contains(decryptionStyle))
                    {
                        switch (decryptionStyle)
                        {
                            case "None":
                                break;
                            case "Reverse Decryption":
                                fileContent = DecryptReverse(fileContent);
                                break;
                            case "Custom Decryption":
                                fileContent = DecryptCustom(fileContent);
                                break;
                            default:
                                fileContent = "Unknown decryption style." + Environment.NewLine + fileContent;
                                break;
                        }
                        returnValue = fileContent;
                    }
                    else
                    {
                        returnValue = "Warning: Your role has read permissions. Not decrypt permissions. " + Environment.NewLine + fileContent;
                    }
                }
                else
                {
                    returnValue = "A " + userRole + " hasn't permission to read a jsonfile.";
                }
            }
            catch (Exception ex)
            {
                returnValue = $"Error while reading a json file: {ex.Message}";
            }
            return returnValue;
        }
    }
}