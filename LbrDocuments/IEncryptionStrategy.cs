﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LbrDocuments
{
    internal interface IEncryptionStrategy
    {
        string DecryptReverse(string text);
        string DecryptCustom(string text);
    }
}
