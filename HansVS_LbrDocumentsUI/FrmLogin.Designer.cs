﻿namespace HansVS_LbrDocumentsUI
{
    partial class FrmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            label1 = new Label();
            cmbUserRoles = new ComboBox();
            btnLogin = new Button();
            btnExit = new Button();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(84, 75);
            label1.Name = "label1";
            label1.Size = new Size(155, 20);
            label1.TabIndex = 0;
            label1.Text = "What is your user role:";
            // 
            // cmbUserRoles
            // 
            cmbUserRoles.FormattingEnabled = true;
            cmbUserRoles.Location = new Point(245, 75);
            cmbUserRoles.Name = "cmbUserRoles";
            cmbUserRoles.Size = new Size(255, 28);
            cmbUserRoles.TabIndex = 1;
            // 
            // btnLogin
            // 
            btnLogin.Location = new Point(84, 129);
            btnLogin.Name = "btnLogin";
            btnLogin.Size = new Size(201, 54);
            btnLogin.TabIndex = 2;
            btnLogin.Text = "Login";
            btnLogin.UseVisualStyleBackColor = true;
            btnLogin.Click += btnLogin_Click;
            // 
            // btnExit
            // 
            btnExit.Location = new Point(299, 129);
            btnExit.Name = "btnExit";
            btnExit.Size = new Size(201, 54);
            btnExit.TabIndex = 3;
            btnExit.Text = "Exit";
            btnExit.UseVisualStyleBackColor = true;
            btnExit.Click += btnExit_Click;
            // 
            // FrmLogin
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(578, 264);
            Controls.Add(btnExit);
            Controls.Add(btnLogin);
            Controls.Add(cmbUserRoles);
            Controls.Add(label1);
            Name = "FrmLogin";
            Text = "FrmLogin";
            Load += FrmLogin_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label label1;
        private ComboBox cmbUserRoles;
        private Button btnLogin;
        private Button btnExit;
    }
}