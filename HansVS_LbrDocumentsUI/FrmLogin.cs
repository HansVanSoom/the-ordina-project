﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace HansVS_LbrDocumentsUI
{
    public partial class FrmLogin : Form
    {
        public string? UserRole { get; set; }
        public FrmLogin()
        {
            InitializeComponent();
        }
        private void FrmLogin_Load(object sender, EventArgs e)
        {
            cmbUserRoles.Items.Clear(); 
            cmbUserRoles.Items.Add("admin");
            cmbUserRoles.Items.Add("user");
        }
        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (cmbUserRoles.SelectedIndex != -1)
            {
                this.UserRole = cmbUserRoles.Text;
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("Your selection is not valid.", "Error");
            }
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
