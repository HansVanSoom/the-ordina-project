﻿namespace HansVS_LbrDocumentsUI
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            label1 = new Label();
            cmbDeCryptionOptions = new ComboBox();
            txtResult = new TextBox();
            btnReadTXT = new Button();
            btnReadXML = new Button();
            btnReadJson = new Button();
            btnLogout = new Button();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(12, 13);
            label1.Name = "label1";
            label1.Size = new Size(170, 20);
            label1.TabIndex = 0;
            label1.Text = "Current decryption used:";
            // 
            // cmbDeCryptionOptions
            // 
            cmbDeCryptionOptions.FormattingEnabled = true;
            cmbDeCryptionOptions.Location = new Point(188, 10);
            cmbDeCryptionOptions.Name = "cmbDeCryptionOptions";
            cmbDeCryptionOptions.Size = new Size(324, 28);
            cmbDeCryptionOptions.TabIndex = 1;
            // 
            // txtResult
            // 
            txtResult.Font = new Font("Courier New", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtResult.Location = new Point(188, 67);
            txtResult.Multiline = true;
            txtResult.Name = "txtResult";
            txtResult.Size = new Size(869, 406);
            txtResult.TabIndex = 2;
            // 
            // btnReadTXT
            // 
            btnReadTXT.Location = new Point(12, 67);
            btnReadTXT.Name = "btnReadTXT";
            btnReadTXT.Size = new Size(170, 64);
            btnReadTXT.TabIndex = 3;
            btnReadTXT.Text = "Read TXT";
            btnReadTXT.UseVisualStyleBackColor = true;
            btnReadTXT.Click += btnReadTXT_Click;
            // 
            // btnReadXML
            // 
            btnReadXML.Location = new Point(12, 137);
            btnReadXML.Name = "btnReadXML";
            btnReadXML.Size = new Size(170, 64);
            btnReadXML.TabIndex = 4;
            btnReadXML.Text = "Read XML";
            btnReadXML.UseVisualStyleBackColor = true;
            btnReadXML.Click += btnReadXML_Click;
            // 
            // btnReadJson
            // 
            btnReadJson.Location = new Point(12, 207);
            btnReadJson.Name = "btnReadJson";
            btnReadJson.Size = new Size(170, 64);
            btnReadJson.TabIndex = 5;
            btnReadJson.Text = "Read JSON";
            btnReadJson.UseVisualStyleBackColor = true;
            btnReadJson.Click += btnReadJson_Click;
            // 
            // btnLogout
            // 
            btnLogout.Font = new Font("Segoe UI", 12F, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point);
            btnLogout.Location = new Point(12, 277);
            btnLogout.Name = "btnLogout";
            btnLogout.Size = new Size(170, 64);
            btnLogout.TabIndex = 6;
            btnLogout.Text = "Logout";
            btnLogout.UseVisualStyleBackColor = true;
            btnLogout.Click += btnLogout_Click;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1069, 485);
            Controls.Add(btnLogout);
            Controls.Add(btnReadJson);
            Controls.Add(btnReadXML);
            Controls.Add(btnReadTXT);
            Controls.Add(txtResult);
            Controls.Add(cmbDeCryptionOptions);
            Controls.Add(label1);
            Name = "Form1";
            Text = "Form1";
            Load += Form1_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label label1;
        private ComboBox cmbDeCryptionOptions;
        private TextBox txtResult;
        private Button btnReadTXT;
        private Button btnReadXML;
        private Button btnReadJson;
        private Button btnLogout;
    }
}