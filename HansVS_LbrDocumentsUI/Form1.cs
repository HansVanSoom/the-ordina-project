
using LbrDocuments;

namespace HansVS_LbrDocumentsUI
{
    public partial class Form1 : Form
    {
        public string? UserRole { get; set; }
        FileReader fr;
        FrmLogin login;
        public Form1()
        {
            InitializeComponent();
            fr = new FileReader();
            login = new FrmLogin();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            login.ShowDialog();
            if (!(login.DialogResult == DialogResult.OK))
            {
                Application.Exit();
            }
            this.Text = "Your current role: " + login.UserRole;
            UserRole = login.UserRole;

            cmbDeCryptionOptions.Items.Add("None");
            cmbDeCryptionOptions.Items.Add("Reverse Decryption");
            cmbDeCryptionOptions.Items.Add("Custom Decryption");
            cmbDeCryptionOptions.SelectedIndex = 0;

            if (UserRole != "admin")
            {
                cmbDeCryptionOptions.Enabled = false;
            }
        }
        private void btnReadTXT_Click(object sender, EventArgs e)
        {
            txtResult.Text = fr.ReadFileTXT(OpenFileDialog(0), UserRole, cmbDeCryptionOptions.Text);
        }
        private void btnReadXML_Click(object sender, EventArgs e)
        {
            txtResult.Text = fr.ReadFileXML(OpenFileDialog(1), UserRole, cmbDeCryptionOptions.Text);
        }
        private void btnReadJson_Click(object sender, EventArgs e)
        {
            txtResult.Text = fr.ReadFileJSON(OpenFileDialog(2), UserRole, cmbDeCryptionOptions.Text);
        }
        private string OpenFileDialog(byte i)
        {
            string returnValue;
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                switch (i)
                {
                    case 0:
                        ofd.Filter = "*text files (*.txt)|*.txt|All files (*.*)|*.*";
                        break;
                    case 1:
                        ofd.Filter = "*xml files (*.xml)|*.xml|All files (*.*)|*.*";
                        break;
                    case 2:
                        ofd.Filter = "*json files (*.json)|*.json|All files (*.*)|*.*";
                        break;
                    default:
                        MessageBox.Show("Document type is not known.");
                        break;
                }
                ofd.ShowDialog();
                returnValue = ofd.FileName;
            }
            catch
            {
                returnValue = "Input is not valid.";
            }
            return returnValue;
        }
        private void btnLogout_Click(object sender, EventArgs e)
        {
            login.ShowDialog();
            if (!(login.DialogResult == DialogResult.OK))
            {
                Application.Exit();
            }
            this.Text = "Your current role: " + login.UserRole;
            UserRole = login.UserRole;

            if (UserRole != "admin")
            {
                cmbDeCryptionOptions.Enabled = false;
            }
            else
            {
                cmbDeCryptionOptions.Enabled = true;
            }
        }
    }
}